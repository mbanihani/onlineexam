import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSidebarModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';

import { ContentModule } from 'app/layout/components/content/content.module';
import { NavbarModule } from 'app/layout/components/navbar/navbar.module';
import { ToolbarModule } from 'app/layout/components/toolbar/toolbar.module';

import { VerticalLayout1Component } from 'app/layout/vertical/layout-1/layout-1.component';
import { Login2Module } from '../../../main/pages/authentication/login-2/login-2.module';
import { Login2Component } from '../../../main/pages/authentication/login-2/login-2.component';
import { TestComponent } from '../../../main/pages/authentication/login-2/test/test.component';
import { UIPageLayoutsModule } from '../../../main/ui/page-layouts/page-layouts.module';
import { UIModule } from '../../../main/ui/ui.module';
import { CardedFullWidthTabbed2Component } from '../../../main/ui/page-layouts/carded/full-width-tabbed-2/full-width-tabbed-2.component';



@NgModule({
    declarations: [
        VerticalLayout1Component
    ],
    imports     : [
        RouterModule.forRoot([
            {
                path      : 'login',
                component : Login2Component
            },
            {
                path      : 'test',
                component :  TestComponent
            }
        ]),

        FuseSharedModule,
        FuseSidebarModule,

        ContentModule,
        NavbarModule,
        ToolbarModule,
        Login2Module,
        ToolbarModule,
        UIPageLayoutsModule,
        UIModule
        ],
    exports     : [
        VerticalLayout1Component
    ]
})
export class VerticalLayout1Module
{
}
