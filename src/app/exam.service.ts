import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable()

export class QuizService {

  constructor(private http: HttpClient) { }

  get(_url: string){
    return this.http.get(_url);
  }

  getAll() {
    return [
      { id: '/assets/data/test.json', name: 'Hermann' }
    ];
  }
}
