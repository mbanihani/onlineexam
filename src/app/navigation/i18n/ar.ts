export const locale = {
    lang: 'ar',
    data: {
        'NAV': {
            'APPLICATIONS': 'تطبيقات',
            'DASHBOARDS'  : 'لوحة التحكم',
            'CALENDAR'    : 'التقويم',
            'ECOMMERCE'   : 'التجارة الالكبرونية',
            'MAIL'        : {
                'TITLE': 'العنوان',
                'BADGE': '15'
            },
            'MAIL_NGRX'        : {
                'BADGE': '13'
            },
            'CHAT'        : 'المحادثة',
            'CONTACTS'    : 'الأسماء',
            'TOOLBAR' : 'القائمة'
        },
        'quiz': {
            'name': 'امتحان هيرمان'
        }
    }
};
