import { InMemoryDbService } from 'angular-in-memory-web-api';


import { SearchFakeDb } from 'app/fake-db/search';
import { IconsFakeDb } from 'app/fake-db/icons';

export class FakeDbService implements InMemoryDbService
{
    createDb(): any
    {
        return {


            // Search
            'search-classic': SearchFakeDb.classic,
            'search-table'  : SearchFakeDb.table,

            // Icons
            'icons': IconsFakeDb.icons
        };
    }
}
