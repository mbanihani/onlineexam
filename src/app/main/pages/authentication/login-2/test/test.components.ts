import { Component, OnInit, OnDestroy } from '@angular/core';
import * as _ from 'lodash';

import { QuizService } from 'app/exam.service';
import { TranslateService } from '@ngx-translate/core';
import { navigation } from 'app/navigation/navigation';
import { FuseConfigService } from '@fuse/services/config.service';


import { Option, Question, Quiz, QuizConfig } from 'app/models';
import { DialogElementsExampleDialog } from 'assets/angular-material-examples/dialog-elements/dialog-elements-example';
import { MatDialog } from '@angular/material';




@Component({
    selector: 'app-test',
    templateUrl: './test.component.html',
    styleUrls: ['./test.component.scss']
})


export class TestComponent implements OnInit {
    quizes: any[];
    quiz: Quiz = new Quiz(null);
    mode = 'quiz';
    quizName: string;
    config: QuizConfig = {
        'allowBack': true,
        'allowReview': true,
        'autoMove': false,  // if true, it will move to next question automatically when answered.
        'duration': 20000,  // indicates the time (in secs) in which quiz needs to be completed. 0 means unlimited.
        'durationq': 20,  // indicates the time (in secs) in which question needs to be completed. 0 means unlimited.
        'pageSize': 1,
        'requiredAll': true,  // indicates if you must answer all the questions before submitting.
        'richText': false,
        'shuffleQuestions': false,
        'shuffleOptions': false,
        'showClock': false,
        'showPager': true,
        'theme': 'none'
    };

    pager = {
        index: 0,
        size: 1,
        count: 1
    };

    // Exam Timer
    timer: any = null;
    startTime: Date;
    endTime: Date;
    ellapsedTime = '00:00';
    duration = '';

    // Question Timer
    timerq: any = null;
    startTimeq: Date;
    endTimeq: Date;
    ellapsedTimeq = '00:00';
    durationq = '';

    // Language
    languages: any;
    navigation: any;
    selectedLanguage: any;

    /**
 * Constructor
 *
 * @param {FuseConfigService} _fuseConfigService
 * @param {TranslateService} _translateService
 */

    constructor(
        private _quizService: QuizService,
        public dialog: MatDialog,
        private _fuseConfigService: FuseConfigService,
        private _translateService: TranslateService
    ) {

        this.languages = [
            {
                id: 'en',
                title: 'English',
                flag: 'us',
                url: 'assets/i18n/en'

            },
            {
                id: 'ar',
                title: 'Arabic',
                flag: 'ar',
                url: 'assets/i18n/ar'
            }
        ];

        this.navigation = navigation;

    }


    ngOnInit() {
        this.quizes = this._quizService.getAll();
        this.quizName = this.quizes[0].id;
        this.loadQuiz(this.quizName);

        // Set the selected language from default languages
        this.selectedLanguage = _.find(this.languages, { 'id': this._translateService.currentLang });
    }

    /**
     * Set the language
     *
     * @param lang
     */
    setLanguage(lang): void {
        // Set the selected language for the toolbar
        this.selectedLanguage = lang;

        // Use the selected language for translations
        this._translateService.use(lang.id);
    }

    loadQuiz(quizName: string) {
        this._quizService.get(quizName).subscribe(res => {
            this.quiz = new Quiz(res);
            this.pager.count = this.quiz.questions.length;
            this.startTime = new Date();
            this.timer = setInterval(() => { this.tick(); }, 1000);
            this.duration = this.parseTime(this.config.duration);

        });

        this.startTimeq = new Date();
        this.timerq = setInterval(() => { this.tickq(); }, 1000);
        this.durationq = this.parseTime(this.config.durationq);
        this.mode = 'quiz';
    }

    openDialog() {
        this.dialog.open(DialogElementsExampleDialog);
    }

    tick() {
        const now = new Date();
        const diff = (now.getTime() - this.startTime.getTime()) / 1000;
        if (diff >= this.config.duration) {
            this.onSubmit();
        }
        this.ellapsedTime = this.parseTime(diff);
    }


    tickq() {
        const now = new Date();
        const diff = (now.getTime() - this.startTimeq.getTime()) / 1000;
        if (diff >= this.config.durationq) {
            this.goTo(this.pager.index + 1);
        }
        this.ellapsedTimeq = this.parseTime(diff);
    }


    parseTime(totalSeconds: number) {
        let mins: string | number = Math.floor(totalSeconds / 60);
        let secs: string | number = Math.round(totalSeconds % 60);
        mins = (mins < 10 ? '0' : '') + mins;
        secs = (secs < 10 ? '0' : '') + secs;
        return `${mins}:${secs}`;
    }

    get filteredQuestions() {
        return (this.quiz.questions) ?
            this.quiz.questions.slice(this.pager.index, this.pager.index + this.pager.size) : [];
    }

    onSelect(question: Question, option: Option) {
        if (question.questionTypeId === 1) {
            question.options.forEach((x) => { if (x.id !== option.id) x.selected = false; });
        }

    }

    goTo(index: number) {
        if (index >= 0 && index < this.pager.count) {
            this.pager.index = index;
            this.mode = 'quiz';
        }
    }

    isAnswered(question: Question) {
        return question.options.find(x => x.selected) ? 'Answered' : 'Not Answered';
    }

    isCorrect(question: Question) {
        return question.options.every(x => x.selected === x.isAnswer) ? 'correct' : 'wrong';
    }

    onSubmit() {
        let answers = [];
        this.quiz.questions.forEach(x => answers.push({ 'quizId': this.quiz.id, 'questionId': x.id, 'answered': x.answered }));

        // Post your data to the server here. answers contains the questionId and the users' answer.
        console.log(this.quiz.questions);
        if (this.onSelect) {
            this.mode = 'result';
        }
    }



}
