import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MatExpansionModule, MatIconModule, MatDialogModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { TestComponent } from 'app/main/pages/authentication/login-2/test/test.component';
import { MaterialModule } from 'app/main/angular-material-elements/material.module';
import {MatRadioModule} from '@angular/material/radio';
import { DialogElementsExampleDialog } from 'assets/angular-material-examples/dialog-elements/dialog-elements-example';

// import ngx-translate and the http loader
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';



import 'hammerjs';



const routes = [
    {
        path     : 'test',
        component: TestComponent
    }
];

@NgModule({
    declarations: [
        TestComponent,
        DialogElementsExampleDialog
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),

        MatExpansionModule,
        MatIconModule,
        MaterialModule,
        MatRadioModule,
        MatDialogModule,


        FuseSharedModule
    ],
    exports     : [
        TestComponent
    ],
    entryComponents: [
        DialogElementsExampleDialog
    ]
})
export class TestModule
{
}

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}