import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule, MatIconModule, MatTabsModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseDemoModule } from '@fuse/components/demo/demo.module';

import { CardedFullWidthTabbed2Component } from 'app/main/ui/page-layouts/carded/full-width-tabbed-2/full-width-tabbed-2.component';


import { FuseSidebarModule } from '@fuse/components';

const routes: Routes = [
    // Carded
    {
        path     : 'page-layouts/carded/full-width-tabbed-2',
        component: CardedFullWidthTabbed2Component
    }
];

@NgModule({
    declarations: [
        CardedFullWidthTabbed2Component
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatIconModule,
        MatTabsModule,

        FuseSidebarModule,
        FuseSharedModule,
        FuseDemoModule
    ],
    exports : [
        CardedFullWidthTabbed2Component
    ]
})
export class UIPageLayoutsModule
{
}
